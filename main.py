import machine, onewire, ds18x20, time, network, ntptime
from umqtt import MQTTClient

sta_if = network.WLAN(network.STA_IF)

ds_pin = machine.Pin(2) # connect data to D4 pin of WeMos D1 mini board


temp = -1000 # if -1000 do not send anything becouse something wrong
can_work = True # for main cycle and exit if no access to autentication data for wifi

mqtt_client = MQTTClient("kitchen_1","192.168.1.36") # MQ broker address

try:
    sid = open('sid.txt').read()
    passwd = open('pass.txt').read()
except:
    print("Something wrong while opening WiFi authenticaton data from files")
    can_work = False

def do_connect_wifi():
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(sid, passwd)
        while not sta_if.isconnected():
            pass
        print('network config:', sta_if.ifconfig())
        try:
            ntptime.settime()
        except:
            print("Something wrong while ntp synchronizing")

def do_connect_ds18x20():
    global temp
    try:
        global ds_sensor
        ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
    except:
        print("Can't initialize OneWire")
    else:    
        roms = ds_sensor.scan()
        print('Found DS devices: ', roms)
        try:
            ds_sensor.convert_temp()
            temp = ds_sensor.read_temp(roms[0])
        except:
            temp = -1000
            print("Something wrong while connecting to sensor")
        else:
            print(str(time.localtime()), ' : ', str(round(temp,1)))
            
def do_connect_mqtt():
    global temp
    if temp != -1000:
        try:
            mqtt_client.connect()
            mqtt_client.publish("sensor/temperature/kitchen/outdoor",str(round(temp,1)))
            mqtt_client.disconnect()
            print("sending dsta ...")
        except:
            print('Something wrong while sending data to MQ broker')

while can_work:
    do_connect_wifi()
    do_connect_ds18x20()
    do_connect_mqtt()
    time.sleep(60)